package com.agileengine;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Solution {

    private static Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static String CHARSET_NAME = "utf8";

    public static void main(String[] args) {

        String originFilePath = args[0];
        String otherSampleFilePath = args[1];
        String targetElementId = args[2];

        Optional<Element> originalElem = findElementById(new File(originFilePath), targetElementId);

        // My solution can be updated by finding similar elements with different tags.
        List<Element> elementsWithTheSameTag = originalElem
                .map(Element::tag)
                .map(Tag::getName)
                .map(tagName -> findElementsByTag(new File(otherSampleFilePath), tagName))
                .orElseGet(Collections::emptyList);

        Element elementWithMaxSimCoef = findElementWithHighestSimCoef(originalElem, elementsWithTheSameTag);
        LOGGER.info("Similar element path is: {}", getPath(elementWithMaxSimCoef));
    }

    private static String getPath(Element elementWithMaxSimCoef) {
        List<Element> elementsFromRoot = new ArrayList<>(elementWithMaxSimCoef.parents());
        elementsFromRoot.add(0, elementWithMaxSimCoef);
        Collections.reverse(elementsFromRoot);
        return elementsFromRoot.stream()
                .map(element -> String.format("%s[%s]", element.tag(), element.siblingIndex()))
                .collect(Collectors.joining("-> "));
    }

    private static Element findElementWithHighestSimCoef(Optional<Element> originalElem, List<Element> elementsWithTheSameTag) {
        if (!originalElem.isPresent()) {
            throw new IllegalArgumentException("Original element is not present");
        }

        if (elementsWithTheSameTag.isEmpty()) {
            throw new IllegalArgumentException("There are no elements with the same tag");
        }

        Element originalElement = originalElem.get();
        Element elementWithMaxSimCoef = elementsWithTheSameTag.get(0);
        long maxSimCoefficient = 0L;

        /* Below is a very bad and naive brute force approach, that can be replaced with search by css query.
         * Now all similarities have the same value, for example both matching IDs and classes add 1 point to
         * similarity coefficient. Mb it makes sense to value different similarities in a different way. For example
         * similar IDs may add 2 points to similarity coefficient while similar classes add only 1 and so on with
         * other attributes.
         */
        for (Element element : elementsWithTheSameTag) {
            List<Attribute> attributes = element.attributes().asList();
            long similarityCoefficient = originalElement.attributes().asList().stream()
                    .filter(attributes::contains)
                    .count();
            LOGGER.info("Element: {}; similarity coefficient: {}", element, similarityCoefficient);
            if (similarityCoefficient > maxSimCoefficient) {
                elementWithMaxSimCoef = element;
                maxSimCoefficient = similarityCoefficient;
            }
        }
        return elementWithMaxSimCoef;
    }

    // DRY is a bit violated here, needs to be refactored
    private static List<Element> findElementsByTag(File file, String tag) {
        try {
            Document doc = Jsoup.parse(
                    file,
                    CHARSET_NAME,
                    file.getAbsolutePath());

            return doc.getElementsByTag(tag);

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", file.getAbsolutePath(), e);
            return Collections.emptyList();
        }
    }

    private static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

}