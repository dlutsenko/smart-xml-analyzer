To execute program run: ``java -jar  <bundled_app>.jar <input_origin_file_path> <input_other_sample_file_path> <id_in_origin_file>``

##### Examples (with outputs):
1. ``java -jar  ./build/libs/ae-backend-xml-java-snippets-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html make-everything-ok-button`` - console output can be found in `./outputs/sample-1-evil-gemini-output.txt`
2. ``java -jar  ./build/libs/ae-backend-xml-java-snippets-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html make-everything-ok-button`` -  console output can be found in `./outputs/sample-2-container-and-clone-output.txt`
3. ``java -jar  ./build/libs/ae-backend-xml-java-snippets-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html make-everything-ok-button`` -  console output can be found in `./outputs/sample-3-the-escape-output.txt`
4. ``java -jar  ./build/libs/ae-backend-xml-java-snippets-0.0.1.jar ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html make-everything-ok-button`` -  console output can be found in `./outputs/sample-4-the-mash-output.txt`